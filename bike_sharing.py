#!/usr/bin/env python

import csv
import cPickle
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
from mpltools import style; style.use('ggplot')
from sklearn import cross_validation

import os
import sys
# https://github.com/jjinking/datsci
if os.getenv('USER') == 'ubuntu':
    sys.path.insert(0, '/home/ubuntu/datsci/')
else:
    sys.path.insert(0, '../../datsci/')
from datsci import dataio, eda, munge

# Data files
fname_train = 'data/train.csv'
fname_train_hour = 'data/train.hour.csv'
fname_train_hour_hashed = 'data/train.hour.hashed.csv'

fname_test = 'data/test.csv'
fname_test_hour = 'data/test.hour.csv'
fname_test_hour_hashed = 'data/test.hour.hashed.csv'

fname_sample_submission = 'data/sampleSubmission.csv'

def single_value_submission(val, outfile):
    '''
    Create submission of a single value
    '''
    with open(fname_sample_submission,'rU') as fin, dataio.fopen(outfile, mode='w') as fout:
        reader = csv.reader(fin)
        writer = csv.writer(fout)
        writer.writerow(reader.next())
        for row in reader:
            row[1] = val
            writer.writerow(row)

def summarize():
    '''
    Summarize train and test data
    '''
    print "train"
    df_train = pd.read_csv(fname_train)
    print eda.summarize_training_data(df_train,
                                      y_name='count',
                                      summary_pkl='summary_train.pkl')[0]

    print "\ntest"
    df_test = pd.read_csv(fname_test)
    print eda.summarize_training_data(df_test,
                                      y_name='windspeed',
                                      summary_pkl='summary_test.pkl')[0]

def datetime2cats():
    '''
    Extract month, day of week, and hour from datetime column
    In the first column, convert 2011-01-20 12:00:00 to something like
    monday, 12
    '''
    def get_month(dt_str):
        '''Extract month from datetime'''
        return datetime.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S').month
    
    def get_hour(dt_str):
        '''Extract hour from datetime'''
        return datetime.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S').hour

    weekdays = ('Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun')
    def get_dayofweek(dt_str):
        '''Extract day of the week from datetime'''
        day_number = datetime.datetime.strptime(dt_str,
                                                '%Y-%m-%d %H:%M:%S').weekday()
        return weekdays[day_number]
    
    # Convert train
    df = pd.read_csv(fname_train)
    df['month'] = df['datetime'].apply(get_month)
    df['hour'] = df['datetime'].apply(get_hour)
    df['day_of_week'] = df['datetime'].apply(get_dayofweek)
    df.drop('datetime', axis=1, inplace=True)
    df.to_csv(fname_train_hour, index=False)

    # Convert test
    df = pd.read_csv(fname_test)
    df['month'] = df['datetime'].apply(get_month)
    df['hour'] = df['datetime'].apply(get_hour)
    df['day_of_week'] = df['datetime'].apply(get_dayofweek)
    df.drop('datetime', axis=1, inplace=True)
    df.to_csv(fname_test_hour, index=False)

def hash_features():
    '''
    Hash categorical cols
    '''
    hashcols = ['month', 'hour', 'day_of_week', 'season', 'weather']
    
    df = pd.read_csv(fname_train_hour)
    df_hashed = munge.hash_features(df, columns=hashcols)
    df_hashed.to_csv(fname_train_hour_hashed, index=False)

    df = pd.read_csv(fname_test_hour)
    df_hashed = munge.hash_features(df, columns=hashcols)
    df_hashed.to_csv(fname_test_hour_hashed, index=False)

def random_forest(outname, ntrees):
    '''
    Run random forest, and export predictions on test data
    '''
    from sklearn.ensemble import RandomForestRegressor

    # Run cross validation
    df = pd.read_csv(fname_train_hour_hashed)
    target = df['count']
    df.drop(['count', 'casual', 'registered'], axis=1, inplace=True)
    regressor = RandomForestRegressor(n_estimators=ntrees, n_jobs=2)
    scores = cross_validation.cross_val_score(regressor,
                                              df,
                                              target,
                                              cv=5)
    with open('submissions/{}.cv_scores.txt'.format(outname), 'wb') as f:
        f.write(str(scores))
    print scores
    
    # Generate submission
    test_data = pd.read_csv(fname_test_hour_hashed)
    submission = pd.read_csv(fname_sample_submission)    
    regressor.fit(df, target)
    submission['count'] = regressor.predict(test_data)
    submission.to_csv('submissions/{}.csv'.format(outname), index=False)

def svr(outname):
    '''
    Run support vector regressor, and export predictions on test data
    '''
    from sklearn.svm import SVR

    # Run cross validation
    df = pd.read_csv(fname_train_hour_hashed)
    target = df['count']
    df.drop(['count', 'casual', 'registered'], axis=1, inplace=True)
    regressor = SVR(kernel='rbf')
    scores = cross_validation.cross_val_score(regressor,
                                              df,
                                              target,
                                              cv=5)
    with open('submissions/{}.cv_scores.txt'.format(outname), 'wb') as f:
        f.write(str(scores))
    print scores
    
    # Generate submission
    test_data = pd.read_csv(fname_test_hour_hashed)
    submission = pd.read_csv(fname_sample_submission)    
    regressor.fit(df, target)
    submission['count'] = regressor.predict(test_data)
    submission.to_csv('submissions/{}.csv'.format(outname), index=False)

def random_forest_separate(outname, ntrees, weight_casual=0.5):
    '''
    Run random forest on casual and registered separately, then combine the
    results
    '''    
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.base import BaseEstimator, RegressorMixin
    
    class MyRegressor(BaseEstimator, RegressorMixin):
        def __init__(self, n_estimators, n_jobs, weight_casual=0.5):
            super(MyRegressor, self).__init__()

            self.n_estimators = n_estimators
            self.n_jobs = n_jobs
            self.weight_casual = weight_casual
            self.regressor_casual = RandomForestRegressor(n_estimators=n_estimators,
                                                          n_jobs=n_jobs)
            self.regressor_registered = RandomForestRegressor(n_estimators=n_estimators,
                                                              n_jobs=n_jobs)
            self.weight_casual = weight_casual
            
        def fit(self, X, y):
            self.regressor_casual.fit(X, y.casual)
            self.regressor_registered.fit(X, y.registered)
            return self
        
        def predict(self, X):
            estimates_casual = self.regressor_casual.predict(X) * weight_casual
            estimates_registered = self.regressor_registered.predict(X) * (1 - weight_casual)
            return estimates_casual + estimates_registered

        def get_params(self, deep=True):
            return {"n_estimators": self.n_estimators,
                    "n_jobs": self.n_jobs,
                    "weight_casual": self.weight_casual}

        def set_params(self, **parameters):
            for parameter, value in parameters.items():
                self.setattr(parameter, value)
    
    # Run cross validation
    train = pd.read_csv(fname_train_hour_hashed)
    casual_registered = train[['casual', 'registered']]
    train.drop(['count', 'casual', 'registered'], axis=1, inplace=True)
    regressor = MyRegressor(n_estimators=ntrees, n_jobs=4, weight_casual=0.5)
    # scores = cross_validation.cross_val_score(regressor,
    #                                           train,
    #                                           casual_registered,
    #                                           cv=5)
    # with open('submissions/{}.cv_scores.txt'.format(outname), 'wb') as f:
    #     f.write(str(scores))
    # print scores
    
    # Generate submission
    test_data = pd.read_csv(fname_test_hour_hashed)
    submission = pd.read_csv(fname_sample_submission)    
    regressor.fit(train, casual_registered)
    submission['count'] = regressor.predict(test_data)
    submission.to_csv('submissions/{}.csv'.format(outname), index=False)
